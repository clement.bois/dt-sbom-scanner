import argparse
import glob
import json
import os
import ssl
import sys
from enum import Enum
from functools import cache
from logging import Logger
from pathlib import Path
from typing import Optional
from xml.etree import ElementTree as ET

import requests

LOGGER = Logger(__name__)


class AnsiColors:
    BLACK = "\033[0;30m"
    RED = "\033[0;31m"
    GREEN = "\033[0;32m"
    YELLOW = "\033[0;33m"
    BLUE = "\033[0;34m"
    PURPLE = "\033[0;35m"
    CYAN = "\033[0;36m"
    WHITE = "\033[0;37m"

    HGRAY = "\033[90m"
    HRED = "\033[91m"
    HGREEN = "\033[92m"
    HYELLOW = "\033[93m"
    HBLUE = "\033[94m"
    HPURPLE = "\033[95m"
    HCYAN = "\033[96m"
    HWHITE = "\033[97m"

    RESET = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


INSECURE_SSL_CTX = ssl.create_default_context()
INSECURE_SSL_CTX.check_hostname = False
INSECURE_SSL_CTX.verify_mode = ssl.CERT_NONE

MIME_APPLICATION_JSON = "application/json"


class DtPermission(str, Enum):
    """Dependency Track permissions.

    See: https://github.com/DependencyTrack/dependency-track/blob/master/src/main/java/org/dependencytrack/auth/Permissions.java#L27"""

    BOM_UPLOAD = "BOM_UPLOAD"
    """Allows the ability to upload CycloneDX Software Bill of Materials (SBOM)"""
    VIEW_PORTFOLIO = "VIEW_PORTFOLIO"
    """Provides the ability to view the portfolio of projects, components, and licenses"""
    PORTFOLIO_MANAGEMENT = "PORTFOLIO_MANAGEMENT"
    """Allows the creation, modification, and deletion of data in the portfolio"""
    VIEW_VULNERABILITY = "VIEW_VULNERABILITY"
    """Provides the ability to view the vulnerabilities projects are affected by"""
    VULNERABILITY_ANALYSIS = "VULNERABILITY_ANALYSIS"
    """Provides the ability to make analysis decisions on vulnerabilities"""
    VIEW_POLICY_VIOLATION = "VIEW_POLICY_VIOLATION"
    """Provides the ability to view policy violations"""
    VULNERABILITY_MANAGEMENT = "VULNERABILITY_MANAGEMENT"
    """Allows management of internally-defined vulnerabilities"""
    POLICY_VIOLATION_ANALYSIS = "POLICY_VIOLATION_ANALYSIS"
    """Provides the ability to make analysis decisions on policy violations"""
    ACCESS_MANAGEMENT = "ACCESS_MANAGEMENT"
    """Allows the management of users, teams, and API keys"""
    SYSTEM_CONFIGURATION = "SYSTEM_CONFIGURATION"
    """Allows the configuration of the system including notifications, repositories, and email settings"""
    PROJECT_CREATION_UPLOAD = "PROJECT_CREATION_UPLOAD"
    """Provides the ability to optionally create project (if non-existent) on BOM or scan upload"""
    POLICY_MANAGEMENT = "POLICY_MANAGEMENT"
    """Allows the creation, modification, and deletion of policy"""

    def __str__(self) -> str:
        return self.name


class DtProjectDef:
    """Dependency Track project definition (either a UUID or name/version)."""

    def __init__(
        self,
        definition: str,
    ):
        self.definition = definition

    @property
    def is_uuid(self) -> bool:
        return self.definition.startswith("#")

    @property
    def uuid(self) -> Optional[str]:
        return self.definition[1:] if self.is_uuid else None

    @property
    def name(self) -> Optional[str]:
        return None if self.is_uuid else self.definition.split("@")[0]

    @property
    def version(self) -> Optional[str]:
        if self.is_uuid:
            return None
        return self.definition.split("@")[1] if "@" in self.definition else None


class Scanner:
    def __init__(
        self,
        base_api_url: str,
        api_key: str,
        project_path: str,
        path_separator: str = "/",
        verify_ssl: bool = True,
    ):
        self.base_api_url = base_api_url
        self.api_key = api_key
        self.project_path = project_path
        self.path_separator = path_separator
        self.verify_ssl = verify_ssl

        self.sbom_count = 0

    @cache
    def get_permissions(self) -> list[DtPermission]:
        return [
            permission["name"]
            for permission in requests.get(
                f"{self.base_api_url}/v1/team/self",
                headers={"X-API-Key": self.api_key, "accept": MIME_APPLICATION_JSON},
                verify=self.verify_ssl,
            ).json()["permissions"]
        ]

    def has_permission(self, perm: DtPermission) -> bool:
        return perm in self.get_permissions()

    # rewinds the given project path and creates a DT project for each non-UUID defined project
    # retunrs the tail project UUID
    @cache
    def get_or_create_project(self, project_path: str, classifier="application") -> str:
        project_path_parts = project_path.split(self.path_separator)
        project_def = DtProjectDef(project_path_parts[-1])
        if project_def.is_uuid:
            print(
                f"- {AnsiColors.YELLOW}{project_path}{AnsiColors.RESET} is UUID: assume exists..."
            )
            return project_def.uuid

        # project is defined by name/version...
        resp = requests.get(
            f"{self.base_api_url}/v1/project",
            headers={"X-API-Key": self.api_key, "accept": MIME_APPLICATION_JSON},
            params={"name": project_def.name},
            verify=self.verify_ssl,
        )
        resp.raise_for_status()
        # find project with matching name/version
        project_versions: list[dict] = resp.json()
        exact_match = next(
            filter(
                lambda prj: prj["name"] == project_def.name
                and prj.get("version") == project_def.version,
                project_versions,
            ),
            None,
        )
        if exact_match:
            # project already exists: replace name with found UUID
            print(
                f"- {AnsiColors.YELLOW}{project_path}{AnsiColors.RESET} found (by name/version): {exact_match['uuid']}..."
            )
            return exact_match["uuid"]
        # if project exists but not the version, we have to CLONE it
        name_match = next(
            filter(
                lambda prj: prj["name"] == project_def.name,
                project_versions,
            ),
            None,
        )
        if name_match:
            print(
                f"- {AnsiColors.YELLOW}{project_path}{AnsiColors.RESET} found sibling (version: {name_match.get('version')}): {name_match['uuid']}..."
            )
            # now create a clone of the project
            resp = requests.put(
                f"{self.base_api_url}/v1/project/clone",
                headers={
                    "X-API-Key": self.api_key,
                    "accept": MIME_APPLICATION_JSON,
                    "content-type": MIME_APPLICATION_JSON,
                },
                json={
                    "project": name_match["uuid"],
                    "version": project_def.version,
                    "includeTags": True,
                    "includeProperties": True,
                    "includeComponents": True,
                    "includeServices": True,
                    "includeAuditHistory": True,
                    "includeACL": True,
                },
                verify=self.verify_ssl,
            )
            try:
                resp.raise_for_status()
                # TODO: clone doesn't return UUID :(
                resp = requests.get(
                    f"{self.base_api_url}/v1/project/lookup",
                    headers={
                        "X-API-Key": self.api_key,
                        "accept": MIME_APPLICATION_JSON,
                    },
                    params={"name": project_def.name, "version": project_def.version},
                    verify=self.verify_ssl,
                )
                resp.raise_for_status()
                # retrieve UUID from response and return
                created_uuid = resp.json()["uuid"]
                print(
                    f"- {AnsiColors.YELLOW}{project_path}{AnsiColors.RESET} {AnsiColors.HGREEN}successfully{AnsiColors.RESET} cloned (from sibling): {created_uuid}"
                )
                return created_uuid
            except requests.exceptions.HTTPError as he:
                print(
                    f"- create {AnsiColors.YELLOW}{project_path}{AnsiColors.RESET} {AnsiColors.HRED}failed{AnsiColors.RESET} (err {he.response.status_code}): {AnsiColors.HGRAY}{he.response.text}{AnsiColors.RESET}",
                )
                raise

        # project does not exist: create it
        data = {
            "name": project_def.name,
            "version": project_def.version,
            "classifier": classifier.upper(),
            "active": True,
        }
        # TODO: externalReferences
        # data["externalReferences"] = [{"type":"vcs","url":project_url}],
        if len(project_path_parts) > 1:
            # project to create is not a root project: retrieve parent
            parent_def = DtProjectDef(project_path_parts[-2])
            if not parent_def.is_uuid:
                # create parent project
                parent_uuid = self.get_or_create_project(
                    self.path_separator.join(project_path_parts[:-1])
                )
                # now parent def must be a UUID
                parent_def = DtProjectDef("#" + parent_uuid)
            # add parent UUID to params
            data["parent"] = {"uuid": parent_def.uuid}

        print(
            f"- {AnsiColors.YELLOW}{project_path}{AnsiColors.RESET} not found: create with params {AnsiColors.HGRAY}{json.dumps(data)}{AnsiColors.RESET}..."
        )
        resp = requests.put(
            f"{self.base_api_url}/v1/project",
            headers={
                "X-API-Key": self.api_key,
                "accept": MIME_APPLICATION_JSON,
                "content-type": MIME_APPLICATION_JSON,
            },
            json=data,
            verify=self.verify_ssl,
        )
        try:
            resp.raise_for_status()
            # retrieve UUID from response and return
            created_uuid = resp.json()["uuid"]
            print(
                f"- {AnsiColors.YELLOW}{project_path}{AnsiColors.RESET} {AnsiColors.HGREEN}successfully{AnsiColors.RESET} created: {created_uuid}"
            )
            return created_uuid
        except requests.exceptions.HTTPError as he:
            print(
                f"- create {AnsiColors.YELLOW}{project_path}{AnsiColors.RESET} {AnsiColors.HRED}failed{AnsiColors.RESET} (err {he.response.status_code}): {AnsiColors.HGRAY}{he.response.text}{AnsiColors.RESET}",
            )
            raise

    def publish(self, sbom_file: Path):
        print(
            f"{AnsiColors.BOLD}📄 SBOM: {AnsiColors.BLUE}{sbom_file}{AnsiColors.RESET}"
        )
        # load the SBOM content
        with open(sbom_file, "r") as reader:
            sbom_content = reader.read()
        sbom_extension = sbom_file.name.split(".")[-1]
        if sbom_extension == "json":
            sbom_json = json.loads(sbom_content)
            # normalize SBOM (shorten IDs, ...)
            # TODO
            sbom_md_cmp = sbom_json.get("metadata", {}).get("component", {})
            sbom_type = sbom_md_cmp.get("type")
            sbom_name = sbom_md_cmp.get("name")
            sbom_version = sbom_md_cmp.get("version")
        elif sbom_extension == "xml":
            sbom_xml = ET.fromstring(sbom_content)
            # normalize SBOM (shorten IDs, ...)
            # TODO
            sbom_md_cmp = sbom_xml.find("{*}metadata/{*}component")
            sbom_type = sbom_md_cmp.get("type") if sbom_md_cmp else None
            sbom_name = sbom_md_cmp.find("{*}name").text if sbom_md_cmp else None
            sbom_version = sbom_md_cmp.find("{*}version").text if sbom_md_cmp else None
        else:
            raise ValueError(f"unsupported SBOM extension: {sbom_extension}")

        file_prefix = sbom_file.name.split(".")[0]
        print(
            f"- file_prefix: {AnsiColors.HGRAY}{file_prefix}{AnsiColors.RESET}; sbom_type: {AnsiColors.HGRAY}{sbom_type}{AnsiColors.RESET}; sbom_name: {AnsiColors.HGRAY}{sbom_name}{AnsiColors.RESET}; sbom_version: {AnsiColors.HGRAY}{sbom_version}{AnsiColors.RESET}"
        )

        # compute the target project path
        project_path = str.format(
            self.project_path,
            file_prefix=file_prefix,
            sbom_type=sbom_type or "unk",
            sbom_name=sbom_name or "unk",
            sbom_version=sbom_version or "",
        )
        print(
            f"- target project: {AnsiColors.YELLOW}{project_path}{AnsiColors.RESET}"
        )

        self.do_publish(sbom_content, project_path, sbom_type)
        self.sbom_count += 1

    def do_publish(
        self, sbom_content: str, project_path: str, sbom_type: str, allow_retry=True
    ):
        project_path_parts = project_path.split(self.path_separator)
        # determine publish params
        params = {}
        project_def = DtProjectDef(project_path_parts[-1])
        if project_def.is_uuid:
            # target project definition is a UUID: nothing more is required
            params["project"] = project_def.uuid
        else:
            # target project definition is a project name: assume exists or set autoCreate with parent if permission PROJECT_CREATION_UPLOAD
            params["projectName"] = project_def.name
            params["projectVersion"] = project_def.version

            if self.has_permission(DtPermission.PROJECT_CREATION_UPLOAD):
                params["autoCreate"] = "true"
                if len(project_path_parts) > 1:
                    parent_def = DtProjectDef(project_path_parts[-2])
                    if parent_def.is_uuid:
                        params["parentUUID"] = parent_def.uuid
                    else:
                        params["parentName"] = parent_def.name
                        params["parentVersion"] = parent_def.version

        # publish SBOM
        print(
            f"- publish params: {AnsiColors.HGRAY}{json.dumps(params)}{AnsiColors.RESET}..."
        )
        resp = requests.post(
            f"{self.base_api_url}/v1/bom",
            headers={"X-API-Key": self.api_key, "accept": MIME_APPLICATION_JSON},
            files={"bom": sbom_content, **params},
            verify=self.verify_ssl,
        )
        try:
            resp.raise_for_status()
            print(
                f"- publish {AnsiColors.HGREEN}succeeded{AnsiColors.RESET}: {AnsiColors.HGRAY}{resp.text}{AnsiColors.RESET}"
            )
        except requests.exceptions.HTTPError as he:
            print(
                f"- publish {AnsiColors.HRED}failed{AnsiColors.RESET} (err {he.response.status_code}): {AnsiColors.HGRAY}{he.response.text}{AnsiColors.RESET}",
            )
            if (
                he.response.status_code == 404
                and self.has_permission(DtPermission.PORTFOLIO_MANAGEMENT)
                and self.has_permission(DtPermission.VIEW_PORTFOLIO)
                and allow_retry
            ):
                # try to create parent projects
                print("- create projects...")
                # replace last path part with project UUID
                # TODO: retrieve classifier from SBOM
                project_path_parts[-1] = "#" + self.get_or_create_project(
                    project_path, sbom_type
                )
                # then retry
                print("- retry publish...")
                self.do_publish(
                    sbom_content,
                    self.path_separator.join(project_path_parts),
                    sbom_type,
                    allow_retry=False,
                )
            else:
                raise

    def scan(self, sbom_patterns: list[str]):
        print(
            f"🗝 API key has permissions: {AnsiColors.BLUE}{', '.join(self.get_permissions())}{AnsiColors.RESET}"
        )
        print()
        if not self.has_permission(DtPermission.BOM_UPLOAD):
            fail(
                "BOM_UPLOAD permission is mandatory to publish SBOM files to Dependency Track server"
            )
        # scan for SBOM files
        for pattern in sbom_patterns:
            for file in glob.glob(pattern):
                self.publish(Path(file))
                print()


def fail(msg: str) -> None:
    print(f"{AnsiColors.HRED}ERROR{AnsiColors.RESET} {msg}")
    sys.exit(1)


def run() -> None:
    # define command parser
    parser = argparse.ArgumentParser(
        prog="sbom-scanner",
        description="This tool scans for SBOM files and publishes them to a Dependency Track server.",
    )
    parser.add_argument(
        "-u",
        "--base-api-url",
        default=os.getenv("DEPTRACK_BASE_API_URL"),
        help="Dependency Track server base API url (includes '/api')",
    )
    parser.add_argument(
        "-k",
        "--api-key",
        default=os.getenv("DEPTRACK_API_KEY"),
        help="Dependency Track API key",
    )
    parser.add_argument(
        "-p",
        "--project-path",
        default=os.getenv("DEPTRACK_PROJECT_PATH"),
        help="Dependency Track target project path to publish SBOM files to (see doc)",
    )
    parser.add_argument(
        "-s",
        "--path-separator",
        default=os.getenv("DEPTRACK_PATH_SEPARATOR", "/"),
        help="Separator to use in project path (default: '/')",
    )
    parser.add_argument(
        "-i",
        "--insecure",
        action="store_true",
        default=os.getenv("DEPTRACK_INSECURE") is not None,
        help="Skip SSL verification",
    )
    parser.add_argument(
        "sbom_patterns",
        nargs="*",
        default=os.getenv(
            "DEPTRACK_SBOM_PATTERNS", "**/*.cyclonedx.json **/*.cyclonedx.xml"
        ).split(" "),
        help="SBOM file patterns to publish (supports glob patterns). Default: '**/*.cyclonedx.json **/*.cyclonedx.xml'",
    )

    # parse command and args
    args = parser.parse_args()

    # check required args
    if not args.base_api_url and not os.getenv("DEPTRACK_BASE_API_URL"):
        fail(
            "Dependency Track server base API url is required (use --base-api-url CLI option or DEPTRACK_BASE_API_URL variable)"
        )
    if not args.api_key and not os.getenv("DEPTRACK_API_KEY"):
        fail(
            "Dependency Track API key is required (use --api-key CLI option or DEPTRACK_API_KEY variable)"
        )
    if not args.project_path and not os.getenv("DEPTRACK_PROJECT_PATH"):
        fail(
            "Dependency Track target project path is required (use --project-path CLI option or DEPTRACK_PROJECT_PATH variable)"
        )

    # print execution parameters
    print("Scanning SBOM files...")
    print(
        f"- base API url   (--base-api-url)  : {AnsiColors.CYAN}{args.base_api_url}{AnsiColors.RESET}"
    )
    print(
        f"- project path   (--project-path)  : {AnsiColors.CYAN}{args.project_path}{AnsiColors.RESET}"
    )
    print(
        f"- path separator (--path-separator): {AnsiColors.CYAN}{args.path_separator}{AnsiColors.RESET}"
    )
    print(
        f"- insecure       (--insecure)      : {AnsiColors.CYAN}{args.insecure}{AnsiColors.RESET}"
    )
    print(
        f"- SBOM file pattern                : {AnsiColors.CYAN}{', '.join(args.sbom_patterns)}{AnsiColors.RESET}"
    )
    print()

    # execute the scan
    scanner = Scanner(
        base_api_url=args.base_api_url,
        api_key=args.api_key,
        project_path=args.project_path,
        path_separator=args.path_separator,
        verify_ssl=not args.insecure,
    )
    scanner.scan(args.sbom_patterns)

    print("Done!")
    print(
        "----------------------------------------------------------------------------------------------"
    )
    print(f"Summary: {scanner.sbom_count} SBOM published")
