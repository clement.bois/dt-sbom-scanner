## [1.1.1](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.1.0...1.1.1) (2024-05-31)


### Bug Fixes

* **deps:** update poetry dependencies ([6c5dce0](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/6c5dce0145ff754f3ba13188a17e3308018e561f))

# [1.1.0](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.0.2...1.1.0) (2024-05-29)


### Features

* support SBOM metadata expressions in project path ([2579e70](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/2579e70813390ce030f765046ba74975bae9be2f))

## [1.0.2](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.0.1...1.0.2) (2024-05-27)


### Bug Fixes

* improve logs and error management ([6c375df](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/6c375df48d27036b01d0ca6b1bc481d3dbf23808))

## [1.0.1](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.0.0...1.0.1) (2024-05-26)


### Bug Fixes

* manage projects creation ([8049980](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/8049980206330d2faf8f9091b2c4308ecf51faa4))

# 1.0.0 (2024-05-25)


### Features

* initial commit ([5471a06](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/5471a06ee63671c752be98e434e96f0d5a061629))
